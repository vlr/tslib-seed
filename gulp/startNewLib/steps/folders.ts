import { projectName } from "./projectName";

const cwd = process.cwd();

export async function goOneDirUp(): Promise<void> {
  process.chdir("..");
}

export async function goIntoLibDir(): Promise<void> {
  process.chdir(projectName());
}

export async function goBackToSeed(): Promise<void> {
  process.chdir(cwd);
}
