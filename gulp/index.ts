export * from "./build";
export * from "./test";
export * from "./publish";
export * from "./startNewLib";

export { fullTest as default } from "./test";

